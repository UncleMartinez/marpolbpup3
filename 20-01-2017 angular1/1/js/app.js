var app = angular.module( 'firstApp', ['ngSanitize'] );

app.controller( 'firstController', [ '$scope', '$log', 'limitToFilter', function( $scope, $log, limitToFilter ) {
    $scope.message = limitToFilter('Wiadomość z pierwszego kontrolera', 20);
    $log.info( 'Message' + $scope.message );
    $log.warn( 'Message(12)' + limitToFilter( $scope.message, 12) );
    
    $scope.myHeaderInfo = 'Moja informacja w headerze.';
}] );

app.controller( 'secondController', function( $scope ) {
    
    $scope.myTxt = '<strong>Mój nowy tekst</strong>';
    
    $scope.settings = {
        firstName: 'Marcin',
        secondName: 'Testowy'
    }
    
    $scope.message = "Wiadomość z drugiego kontrolera";
    
    $scope.showMsg = function() {
        alert( $scope.message );
    }
    
    $scope.users = [
        'Bill Gates',
        'Elon Musk',
        'Gaben',
        'Antoni Macierewicz',
        'Hanna Mostowiak'
    ];
    
    $scope.access = [ 'read', 'write', 'admin' ];
    
    $scope.clearAccess = function() {
        $scope.access = [];
    }
    
    $scope.addAccess = function() {
        $scope.access = ['read', 'write', 'admin'];  
    }
    
    $scope.showCommentModel = function() {
        alert( $scope.comment );
    }
    
});

app.controller( 'thirdController', function() {
   this.info = "Wiadomość z trzeciego kontrolera"; 
});

app.controller('usersController', function( $scope ) {
    $scope.users = [
        {name: 'Marcin', active: true, role: 'admin'},
        {name: 'Antoni', active: true, role: 'user'},
        {name: 'Paweł', active: false, role: 'user'},
        {name: 'Madzia', active: true, role: 'admin'},
        {name: 'Asia', active: false, role: 'user'},
        {name: 'Natalia', active: true, role: 'user'},
        {name: 'Maciek', active: true, role: 'user'}
    ]; 
    
    $scope.styles = {
        fontWeight: 'bold',
        color: 'red'
    }
});
