var app = angular.module( 'firstApp', [] );

app.controller( 'secondController', function( $scope ) {
    
    $scope.settings = {
        firstName: 'Marcin',
        secondName: 'Testowy'
    }
    
    $scope.message = "Wiadomość z drugiego kontrolera";
    
    $scope.showMsg = function() {
        alert( $scope.message );
    }
    

    
    $scope.access = [ 'read', 'write', 'admin' ];
    
    $scope.clearAccess = function() {
        $scope.access = [];
    }
    
    $scope.addAccess = function() {
        $scope.access = ['read', 'write', 'admin'];  
    }
    
    $scope.showCommentModel = function() {
        alert( $scope.comment );
    }
    
});

app.controller('usersController', function( $scope ) {
    $scope.users = [
        {name: 'Marcin', active: true, role: 'admin'},
        {name: 'Antoni', active: true, role: 'user'},
        {name: 'Paweł', active: false, role: 'user'},
        {name: 'Madzia', active: true, role: 'admin'},
        {name: 'Asia', active: false, role: 'user'},
        {name: 'Natalia', active: true, role: 'user'},
        {name: 'Maciek', active: true, role: 'user'}
    ]; 
    
    $scope.styles = {
        fontWeight: 'bold',
        color: 'red'
    }
});
