var app = angular.module( 'firstApp', [] );

app.controller( 'formController', [ '$scope', '$log', function( $scope, $log ) {
    
    $scope.showUserData = function() {
        $log.info( 'Imię: ' + $scope.name + '\n'
                 + 'Nazwisko: ' + $scope.surname + '\n'
                 + 'E-mail: ' + $scope.mail + '\n'
                 + 'Hasło: ' + '*'.repeat( $scope.pass.length ));
    }
    
}] );
