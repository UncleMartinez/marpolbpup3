var app = angular.module( 'GymForProgrammers', ['ngRoute'] );

app.config(function($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    
    $routeProvider
        .when('/', {
            templateUrl: 'views/login.html',
            controller: 'loginController'
        })
        .when('/registration', {
            templateUrl: 'views/registration.html',
            controller: 'registrationController'
        })
        .when('/profile', {
            templateUrl: 'views/profile.html',
            controller: 'profileController'
        })
        .when('/foods', {
            templateUrl: 'views/foods.html',
            controller: 'foodsController'
        })
        .otherwise({
            redirectTo: '/'
        });
});

app.controller('loginController', function($scope, $routeParams, $location) {
    $('body').removeClass();
    $('body').addClass('loginBody');
    
    $scope.login = function() {
        $location.url('/profile');
    }
});

app.controller('registrationController', function($scope, $routeParams, $location) {
    $('body').removeClass();
    $('body').addClass('registrationBody');
        
    $scope.register = function() {
        alert('Zarejestrowano użytkownika');
        $location.url('/');
    }
});


app.controller('profileController', function($scope, $routeParams) {
    $('body').removeClass();
    $('body').addClass('profileBody');
    
    $scope.showUserDetails = function() {
        $("#panel").slideToggle("slow");
    }
    
    $scope.showPersonActivityDesc = function() {
        $("#personActivityDesc").slideToggle("slow");
    }
    
    $scope.showSaveAndCancel = function() {
        $("#editProfileBtn").fadeOut("slow");
        $(".saveAndCancel").fadeIn("slow");
    }
    
    $scope.showSaveBtn = function() {
        $(".saveAndCancel").fadeOut("slow");
        $("#editProfileBtn").fadeIn("slow");
    }
});


app.controller('foodsController', function($scope, $routeParams) {
    $('body').removeClass();
    $('body').addClass('foodsBody');
    
    $('#foodsTable').DataTable( {
        data:           "/JSONdatabase/foodsDB.json",
        deferRender:    true,
        scrollY:        200,
        scrollCollapse: true,
        scroller:       true,
        responsive:     true,
        "language": {
                "url": "/lang/polish.lang"
            }
    } );
    
    
});