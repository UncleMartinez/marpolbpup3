$(document).ready(function() { 

	var users = [
        {name: 'Marcin', surname: 'Pol', pesel: '11111111111', balance: 123.12, currency: 'PLN'},
        {name: 'Jan', surname: 'Pawel', pesel: '22222222222', balance: 123.12, currency: 'PLN'},
        {name: 'Jarek', surname: 'Nowak', pesel: '33333333333', balance: 123.12, currency: 'PLN'}
    ];
    
    $('#signBtn').click(function() {
        var pesel = $('#PESELInp').val(),
            userID = checkIfUserExist(pesel);
        if(userID>=0) {
            $('.navbar-brand').html('Witaj ' + users[userID].name + ' ' + users[userID].surname);
            $('.navbar-form').html('');
            $('.navbar-form').html(
                "<button type='submit' class='btn btn-danger' id='logoutBtn'>Wyloguj</button>"
            );
        } else if(userID<0) {
            alert('Nie ma takiego numeru PESEL');
        }
        return false;
    });
    //TODO: I KNOW I REPEAT YOURSELF :( 
    $('#logoutBtn').click(function() {
        $('.navbar-brand').html("Bank Korporejszyn");
//        $('.navbar-form').html(
//            "<div class='form-group'>" +
//            '<input type="text" placeholder="PESEL" class="form-control" id="PESELInp">' +
//            '</div>' +
//            '<button type="submit" class="btn btn-success" id="signBtn">Zaloguj</button>"'
//        );
        alert('Wylogowano!');
        return false;
    });
    
    function checkIfUserExist(userPESEL){
        for(var i=0; i<users.length; i++) {
            if(users[i].pesel === userPESEL) {
                return i;    
            } 
        }
        return -1;
    };
    
});