$(document).ready(function() {

    $('.btn-check').click(function() {
        var imieZInputa = $('.inpImie').val();
        var nazwiskoZInputa = $('.inpNazwisko').val();
        $.ajax({
            url: 'json.php',
            dataType: 'json',
            data: { imie: imieZInputa, nazwisko: nazwiskoZInputa },
            success: function(data) {
                var ret = '';
                if(data.status == 'ok') {
                    ret += data.imie + ' ' + data.nazwisko;
                }
                if(data.status == 'error') {
                    ret += 'Błąd';
                }
                if(data.password) {
                    ret += '</br>';
                    ret += '*'.repeat(data.password.length);
                }
                $('.usersContainer').html(ret);
            }
       });
    });
});