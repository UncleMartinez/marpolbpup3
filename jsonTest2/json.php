<?php

$arr = array (
    'status' => 'error'
);

if( isset($_REQUEST['imie']) && isset($_REQUEST['nazwisko']) ) {
    $arr['status'] = 'ok';
    $arr['imie'] = $_REQUEST['imie'];
    $arr['nazwisko'] = $_REQUEST['nazwisko'];
    if( strtolower($_REQUEST['imie']) == 'admin') {
        $arr['password'] = 'superTajneHaslo';
    }
}

echo json_encode ( $arr );