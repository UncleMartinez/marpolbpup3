var app = angular.module('simpleApp', []);

app.controller('simpleController', function($scope) {
    $scope.movies = [     
	   { title: 'Przykładowy film' , description: 'Przykładowy opis testowego filmu', lasts: 147, year: 2010, type: 'movie' },
	   { title: 'Przykładowy film2' , description: 'Przykładowy opis testowego filmu22', lasts: 47, year: 2011, type: 'show' },
	   { title: 'Przykładowy film3' , description: 'Przykładowy opis testowego filmu33', lasts: 117, year: 2012, type: 'movie' }
    ]; 
    
    $scope.showMovie = function(mov) {
        $('.oneMovie').html( mov.description );
    }
});