var app = angular.module( 'firstApp', [] );

app.controller( 'firstController', [ '$scope', '$filter', 'orderByFilter', function( $scope, $filter, orderByFilter ) {
    
    $scope.movies = [
        { title: 'Lucyfer' , description: 'Nieszczęśliwy i znudzony swoim bytem Lucyfer Morningstar porzuca funkcję Władcy Piekieł i udaje się do Los Angeles, gdzie zostaje właścicielem luksusowego klubu nocnego.', lasts: 42, year: 2016, type: 'serial', image: 'http://1.fwcdn.pl/po/25/20/742520/7729448.2.jpg' },
        { title: 'Gra o tron' , description: 'Adaptacja sagi George\'a R.R. Martina. W królestwie Westeros walka o władzę, spiski oraz zbrodnie są na porządku dziennym.', lasts: 60, year: 2011, type: 'serial', image: 'http://1.fwcdn.pl/po/68/48/476848/7728238.2.jpg' },
        { title: 'The Walking Dead' , description: 'Świat opanowały zombie. Grupka ocalałych szuka bezpiecznego schronienia.', lasts: 45, year: 2010, type: 'serial', image: 'http://1.fwcdn.pl/po/70/35/547035/7772556.2.jpg' },
        { title: 'Avatar' , description: 'Jake, sparaliżowany były komandos, zostaje wysłany na planetę Pandora, gdzie zaprzyjaźnia się z lokalną społecznością i postanawia jej pomóc.', lasts: 162, year: 2009, type: 'film', image: 'http://1.fwcdn.pl/po/91/13/299113/7332755.2.jpg' },
        { title: 'Kroniki Riddicka' , description: 'Armia okrutnych Necromongerów najeżdża galaktykę. Jedyną istotą, która może im się przeciwstawić, jest awanturnik i ścigany więzień Riddick, mający dar widzenia w ciemności.', lasts: 119, year: 2004, type: 'film', image: 'http://1.fwcdn.pl/po/61/28/106128/7527054.2.jpg' }
    ];
    
    
    $scope.actualMovie = 0;
//    $scope.showMovie($scope.actualMovie);
        
    $scope.showMovie = function(nr) { 
        $('.oneMovie').html( 
            '<h3>' + $scope.movies[nr].title + '</h3>' +
            '<p> Typ: ' + $scope.movies[nr].type + ', Rok: ' + $scope.movies[nr].year + ', Czas trwania: ' + $scope.movies[nr].lasts + 'min</p>' +
            '<p>' + $scope.movies[nr].description + '</p>'
        );
    }
    
    $scope.showImage = function(nr) {
        $('.movieImg').html(
            '<img src="' + $scope.movies[nr].image + '"></img>'
        );
    }
    
    
    $scope.sendIndex = function(index) {
        $scope.actualMovie = index;
        $scope.showMovie($scope.actualMovie);
        $scope.showImage($scope.actualMovie);

    }
    
    $('.leftBtn').on('click', function() {
        if($scope.actualMovie>0) $scope.actualMovie--;
        $scope.showMovie($scope.actualMovie);
        $scope.showImage($scope.actualMovie);

    });
    
        $('.rightBtn').on('click', function() {
        if($scope.actualMovie<$scope.movies.length-1) $scope.actualMovie++;
        $scope.showMovie($scope.actualMovie);
        $scope.showImage($scope.actualMovie);
    });
    
    $scope.sortOrder = false;
    
    $scope.sortMovies = function( sort ) {
        $scope.movies = orderByFilter($scope.movies, sort, $scope.sortOrder);
        $scope.sortOrder = ($scope.sortOrder) ? false : true;
    $scope.showMovie($scope.actualMovie);
    $scope.showImage($scope.actualMovie);
        
        if($('.sortBtn').hasClass('glyphicon-sort-by-alphabet-alt')) {
            $('.sortBtn').removeClass('glyphicon-sort-by-alphabet-alt');
            $('.sortBtn').addClass('glyphicon-sort-by-alphabet');
        } 
        else {
            $('.sortBtn').removeClass('glyphicon-sort-by-alphabet');
            $('.sortBtn').addClass('glyphicon-sort-by-alphabet-alt');
        }
    }
    
    $scope.sendNewMovie = function() {
        $scope.movies.push({
            title: $scope.title , description: $scope.description, lasts: $scope.lasts, year: $scope.year, type: $scope.type, image: $scope.image 
        });
    }
    
    $scope.isSelected = function(index){
        return $scope.actualMovie === index;
    }
    
    $scope.init = function(){
        $scope.sortMovies('title');
        $scope.showMovie($scope.actualMovie);
        $scope.showImage($scope.actualMovie);

    }
    $scope.init();
    

}] );