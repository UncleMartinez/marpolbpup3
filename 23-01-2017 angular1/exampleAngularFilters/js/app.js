var app = angular.module('filterApp', []);

app.controller('filterController', ['$scope', '$filter', 'orderByFilter', function($scope, $filter, orderByFilter) {
    $scope.movies = [     
	   { title: 'Szklana pułapka' , description: 'Przykładowy opis testowego filmu 1', lasts: 147, year: 2010, type: 'movie' },
	   { title: 'Przykładowy film2' , description: 'Przykładowy opis testowego filmu 2', lasts: 47, year: 2011, type: 'show' },
	   { title: 'Moda na sukces' , description: 'Przykładowy opis testowego filmu 3', lasts: 147, year: 2012, type: 'movie' },
	   { title: 'Jack Reacher' , description: 'Przykładowy opis testowego filmu 4', lasts: 147, year: 2012, type: 'movie' },
	   { title: 'Ojciec Mateusz' , description: 'Przykładowy opis testowego filmu 5', lasts: 147, year: 2012, type: 'movie' }
    ];
    
    $scope.sortOrder = true;
    $scope.sortMovies = function( sort ) {
        $scope.movies = orderByFilter($scope.movies, sort, $scope.sortOrder);
        $scope.sortOrder = ($scope.sortOrder) ? false : true;
    }
}]);