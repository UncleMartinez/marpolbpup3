var Template = function() {
    
    this.currentTemplate = '';
    
    this.generatePost = function(title, body) {

        var myTemplate = '<div class="row">'
            + '<div class="col-md-12">'
            + '<div class="well">'
            + '<h2>' + title + '</h2>'
            + '<p>' + body + '</p>'
            + '</div>'
            + '</div>'
            + '</div>';
        this.currentTemplate = myTemplate;
        return this;
        //return myTemplate;
    }

    this.addPost = function() {
        $( this.currentTemplate ).appendTo('.container');
        return this;
    }
    
    this.generateFilledTemplate = function( template, data ) {
        var keys = Object.keys(data);
        
            for(var i in keys) {
                template = template.replace('[% ' + keys[i] + ' %]', data[ keys[i] ]);
            }
        return template; // {{ zmienna }} -> [% zmienna %]
    }
    
    this.generateFilledTemplateArray = function(template, data) {
        var myTemporaryTemplate = '';
        for(var i=0; i<data.length; i++) {
            myTemporaryTemplate += this.generateFilledTemplate(template,data[i]);
        }
        return myTemporaryTemplate;
    }
    
    this.generate = function(template, data) {
        if(Array.isArray(data)) {
            this.currentTemplate =
            this.generateFilledTemplateArray(template, data);
        } else {
            this.currentTemplate =
            this.generateFilledTemplate(template, data);
        }
        return this;
    }

}

//var t = new Template();

//t.addPost( t.generatePost( 'Tytuł', 'Treść' ) );
//t.addPost( t.generatePost( 'Inny Tytuł', ' Inna Treść' ) );

var temp = '<div class="row">'
            + '<div class="col-md-12">'
            + '<div class="well">'
            + '<h2>[% title %]</h2>'
            + '<p>[% post %]</p>'
            + '<p><small>[% author %]</small> - [% data %]</p>'
            + '</div>'
            + '</div>'
            + '</div>';

var myData = {
    title: 'Mój przykładowy post z generatora.',
    post: 'Praesent efficitur, metus non tincidunt commodo, justo quam laoreet sem, non cursus magna nisl nec diam. Fusce suscipit elit sit amet elit pretium malesuada. Curabitur diam massa, sollicitudin ac hendrerit nec, interdum ut odio. Nulla tincidunt neque molestie congue pellentesque. Ut vehicula dapibus pharetra. Donec laoreet ipsum lorem, id fringilla massa mattis vitae. In tristique ut velit a fermentum. Suspendisse cursus pulvinar mattis. Nunc sed fringilla ante, quis semper ipsum. Phasellus aliquam maximus congue. Suspendisse ornare leo id erat dignissim, in gravida libero pellentesque. Nullam ullamcorper dolor eu dui elementum ultricies. In ut sem sollicitudin nulla suscipit bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.',
    author: 'paweł',
    data: '19.01.2017'
};

var myDataAll = [
    {
        title: 'Mój pierwszy post',
        post: 'Praesent efficitur, metus non tincidunt commodo.',
        author: 'Marcin',
        data: '19.01.2017'
    },
    {
        title: 'Mój drugi post',
        post: 'Lorem ipsum dolor sit amet.',
        author: 'Marcin',
        data: '19.01.2017'
    },
    {
        title: 'Mój trzeci post',
        post: 'Lorem ipsum dolor sit amet.',
        author: 'Marcin',
        data: '19.01.2017'
    }

];

var t = new Template();
t.generatePost( 'Tytuł', 'Treść' ).addPost().generate(temp,myDataAll).addPost().generate(temp,myData).addPost();
//t.addPost( t.generate( temp, myData) ); 
//t.addPost( t.generate( temp, myDataAll));


//$(document).ready(function() {
//    
//    
//    var Post = function(title,message) {
//        
//        this.generatePost = function() {
//            this.post = 
//                '<div class="row">' +
//                    '<div class="col-md-12">' +
//                        '<div class="well">' +
//                            '<h2>'+ title +'</h2>' +
//                            '<p>'+ message +'</p>' +
//                        '</div>' +
//                    '</div>' +
//                '</div>'; 
//            return this.post;
//        }
//        
//        this.addPost = function() {
//            $('.container').append(this.post);
//        }
//        
//        this.init = function() {
//            this.generatePost();
//            this.addPost();
//        }
//        
//        this.init();
//    }
//    
//    var myPost1 = new Post('TITLE1','Lorem ipsum 11111');
//    var myPost2 = new Post('TITLE2','Lorem ipsum 22222');
//    var myPost3 = new Post('TITLE3','Lorem ipsum 33333');
//
//
//});