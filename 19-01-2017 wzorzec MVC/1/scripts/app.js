var Dice = function(arg) {
    
    this.rollTable = [];
    this.countedVars = { };
    this.bars = '';
    
    this.diceRoll = function() {
        return Math.floor((Math.random() * 6) + 1);
    }
    
    this.generateRollTable = function() {
        for(var i=0; i<arg; i++){
            this.rollTable.push(this.diceRoll());
        }
    }
    
    this.countVars = function() {
        for(var i=0; i<this.rollTable.length; i++){
            var num = this.rollTable[i].toString();
            if( this.countedVars[ num ] ) {
                this.countedVars[ num ]++;
            } else {
                this.countedVars[ num ] = 1;
            }
        }
    }
    
    this.generateBars = function() {
        for(var i in this.countedVars) {
            var percent = this.countedVars[i] * 100 / this.rollTable.length;
            this.generateBar(percent);
        }
        $('.col-md-12').html(this.bars);
    }
    
    this.generateBar = function(percent) {
        this.bars += '<div class="progress">' +
                    '<div class="progress-bar" role="progressbar" aria-valuenow="'+percent+'" aria-valuemin="0" aria-valuemax="100" style="width: '+percent+'%;">' + percent.toFixed() + '</div></div>';
    }
    
    this.init = function() {
        this.generateRollTable();
        this.countVars();
        this.generateBars();
    }
    
    this.init();
}

var myDice = new Dice(70);
console.log(myDice.countedVars);


//MOJE
//$(document).ready(function() {
//
//    function Dice(){
//        this.numbers = [];
//        this.myUniqueArray = {};
//        
//        this.generateNumber = function() {
//            var rand1To6 = Math.floor(Math.random() * 6) + 1;
//            return rand1To6;
//        }
//
//        this.generateRandomNumbers = function(arg) {
//            for(var i=0; i<arg; i++){
//                this.numbers.push(this.generateNumber());
//            }
//            return this.numbers;
//        }
//
//        this.countNumbers = function() {
//                for(var i in this.numbers) {
//                this.myUniqueArray[this.numbers[i]] = 
//                    this.myUniqueArray[this.numbers[i]] ? this.myUniqueArray[this.numbers[i]]+1 : 1;
//            }
//            console.log(this.numbers, this.myUniqueArray);
//        }
//        
//        this.showPercent = function() {
//            var all = this.numbers.length;
//            var math = this.myUniqueArray['1'] * 100 / all;
//            console.log( math.toFixed(2) + ' %');
//        }
//        
//        this.showProgressBar = function() {
//            $('#container').html(
//                '<div class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">60%</div></div>'
//            );
//
//        }
//        
//    }
//    
//    var Dice1 = new Dice();
//    Dice1.generateRandomNumbers(10);
//    Dice1.countNumbers();
//    Dice1.showPercent();
//    Dice1.showProgressBar();
//    
//});
