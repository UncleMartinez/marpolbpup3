var app = angular.module( 'firstApp', ['ngRoute'] );

app.controller( 'firstController', [ '$scope', '$filter', 'orderByFilter', '$http', '$routeParams', '$log', function( $scope, $filter, orderByFilter, $http, $routeParams, $log ) {
    
    
    $scope.movies = [];
    
    var url = '//localhost/24-01-2017/moviesDB/json.php';
    
    
    $http({
        url: url, 
        method:'GET',
        dataType: 'json',
        params: {
            action: 'show',
            id: $routeParams.id
        }
    }).then( function(data) {
        $scope.actualMovie = 0;
        
        $scope.movies = data.data.posts;
        $scope.sortMovies('title');
        $scope.currentMovie = $scope.movies[ $scope.actualMovie ];
    }, function(data) {
        alert('błąd');
    });
    
    
    $scope.sendIndex = function(index) {
        $scope.actualMovie = index;
        $scope.currentMovie = $scope.movies[ $scope.actualMovie ];
    }
    
    
    $scope.prev = function() {
        if($scope.actualMovie > 0) {
            $scope.currentMovie = $scope.movies[ --$scope.actualMovie ];
        }
    };
    
    
    $scope.next = function() {
        if($scope.actualMovie<$scope.movies.length-1) $scope.currentMovie = $scope.movies[ ++$scope.actualMovie ];
    };
    
    
    $scope.sortOrder = false;
    
    
    $scope.sortMovies = function( sort ) {
        $scope.movies = orderByFilter($scope.movies, sort, $scope.sortOrder);
        $scope.sortOrder = ($scope.sortOrder) ? false : true;
        $scope.currentMovie = $scope.movies[ $scope.actualMovie ];
        if($('.sortBtn').hasClass('glyphicon-sort-by-alphabet-alt')) {
            $('.sortBtn').removeClass('glyphicon-sort-by-alphabet-alt');
            $('.sortBtn').addClass('glyphicon-sort-by-alphabet');
        } 
        else {
            $('.sortBtn').removeClass('glyphicon-sort-by-alphabet');
            $('.sortBtn').addClass('glyphicon-sort-by-alphabet-alt');
        }
    }
    
    
    $scope.isSelected = function(index){
        return $scope.actualMovie === index;
    }  
    
    
    $scope.addMovie = function() {
        $http({
            url: url, 
            method:'POST',
            dataType: 'json',
            params: {
                action: 'add',
                title: $scope.title,
                description: $scope.description,
                lasts: $scope.lasts,
                year: $scope.year,
                type: $scope.type,
                image: $scope.image
            }
        }).then( function(data) {
            console.log(data.data.status)
        }, function(data) {
            alert('błąd');
        });
    }

    
}] );