var app = angular.module('simpleRouterApp', ['ngRoute']);

app.controller('mainController', function($scope, $routeParams) {
    $scope.title = 'Przykładowa strona z routerem';
});

app.controller('userController', function($scope, $routeParams) {
    $scope.users = [
        { name: 'pawel', surname: 'testowy' },
        { name: 'kazimierz', surname: 'testowy' },
        { name: 'jan', surname: 'testowy' }
    ];
    if( $scope.users[ $routeParams.id - 1] ) {
        $scope.user = $scope.users[ $routeParams.id - 1 ];
    } else {
        $scope.error = 'Taki użytkownik nie istnieje';
    }
});

app.config(function($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    
    $routeProvider
        .when('/', {
            templateUrl: 'views/main.html',
            controller: 'mainController'
        })
        .when('/user/:id', {
            templateUrl: 'views/user.html',
            controller: 'userController'
        })
        .otherwise({
            redirectTo: '/'
        });
});